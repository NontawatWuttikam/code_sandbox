`timescale 1ns/ 1ns
`include "moore_state.v"

module moore_state_tb;

    reg x,clock,reset;
    wire z;

moore_state uut(z,x,clock,reset);

always begin
    #50 clock = !clock ;
end

initial begin

    $dumpfile("moore_state.vcd");
    $dumpvars(0, moore_state_tb);

    clock = 1;
    reset = 0;
    #100;
    reset = 1;
    x = 1;
    #100;
    x = 0;
    #100;
    x=1;
    #100;
    x=1;
    #100;
    x=1;
    #100
    x=0;
    #100


    $display("Test Complete");
    $finish;

end

endmodule