module shift_reg(A,E,rst,clk);

    input E, rst, clk;
    output A;
    reg A,B,C,D;
    always @ (posedge clk or posedge rst)
    begin
      if(rst) {A,B,C,D} = 4'b0000;
      else
      begin
        D = E;
        C = D;
        B = C;
        A = B;
      end
    end
endmodule