module moore_state(z,x,clock,reset);
    
    output z;
    input x, clock, reset;
    reg [1:0] state;
    reg z;

    parameter s0 = 2'b00;
    parameter s1 = 2'b01;
    parameter s2 = 2'b10;
    parameter s3 = 2'b11;

    always @(posedge clock, negedge reset)
    begin
      if(~reset) state = s0;

      case(state)
        s0 :
            if (x==0) begin 
                state = s0;
                z = 0;
            end
            else if (x==1) begin 
                state = s1;
                z = 0;
            end
        s1 :
            if (x==0) begin 
                state = s1;
                z = 0;
            end
            else if (x==1) begin 
                state = s2;
                z = 0;
            end

        s2 :
            if (x==0) begin 
                state = s2;
                z = 0;
            end
            else if (x==1) begin 
                state = s3;
                z = 1;
            end

        s3 :
            if (x==0) begin 
                state = s3;
                z = 1;
            end
            else if (x==1) begin 
                state = s0;
                z = 0;
            end
      endcase
    end


endmodule