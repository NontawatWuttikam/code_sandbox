`timescale 1ns/ 1ns
`include "shift_reg.v"

module shift_reg_tb;

    reg E,clk,rst;
    wire A;

shift_reg uut(A,E,rst,clk);

always begin
    #50 clk = !clk ;
end

initial begin

    $dumpfile("shift_reg_tb.vcd");
    $dumpvars(0, shift_reg_tb);

    E=0;
    rst = 1;
    clk = 0;
    #20;
    rst = 0;
    #80;
    E=1;
    #100;
    E=0;
    #700;

    $display("Test Complete");
    $finish;

end

endmodule