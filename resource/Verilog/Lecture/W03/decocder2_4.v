module decoder2_4 (y,E,A1,A0);

    output [3:0] y;
    input E, A1, A0;

    assign y[0] = ~A0 & ~A1 & E;
    assign y[1] = A0 & E & ~A1;
    assign y[2] = ~A0 & E & A1;
    assign y[3] = A0 & E & A1;
    
endmodule