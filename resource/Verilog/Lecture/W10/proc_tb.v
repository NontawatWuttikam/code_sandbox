`timescale 1ns/1ns
`include "proc.v"
module proc_tb;

reg clock, reset;
wire [7:0] PC, AC, IR;
wire [7:0] rom_address, ram_address;
wire [7:0] rom_data_out, ram_data_out;
wire ram_write;
wire [4:0] state;

proc uut( state, clock,reset,
            PC, AC, IR,
            rom_address, ram_address,
            rom_data_out, ram_data_out,
            ram_write);

always begin #50; clock = !clock; end 
initial begin

    $dumpfile("proc.vcd");
    $dumpvars(0, proc_tb);

    clock = 0;
    reset = 0;

    #100;
    reset = 1;
    #2000;

    $display("Work complete");
    $finish;
end
endmodule