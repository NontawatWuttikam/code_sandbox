module proc(state, clock,reset,
            PC, AC, IR,
            rom_address, ram_address,
            rom_data_out, ram_data_out,
            ram_write);

    input clock, reset;
    output [7:0] PC, AC, IR;
    output [7:0] rom_address, ram_address;
    output [7:0] rom_data_out, ram_data_out;
    output ram_write;

    reg [7:0] PC, AC, IR;
    reg [7:0] rom_address, ram_address;
    reg [7:0] ram_data_in;
    reg ram_write;

    output [4:0] state;
    reg [4:0] state;

    parameter reset_cpu = 0;
    parameter fetch = 1;
    parameter decode = 2;

    parameter execute_NOP = 3;
    parameter execute_INC_A = 4;
    parameter execute_ADD_I = 5;
    parameter execute_ADD_M1 = 6;
    parameter execute_ADD_M2 = 7;
    parameter execute_MOV_A_TO_M1 = 8;
    parameter execute_MOV_A_TO_M2 = 9;
    parameter execute_MOV_I_TO_A = 10;
    parameter execute_JMP1 = 12;
    parameter execute_JMP2 = 13;
    parameter execute_JZ1 = 14;
    parameter execute_JZ2 = 15;
    parameter execute_MOV_M_TO_A1 = 16;
    parameter execute_MOV_M_TO_A2 = 17;

    rom_memory rom_mem(rom_address, rom_data_out, clock);
    ram_memory ram_mem(ram_address, ram_data_out, ram_data_in, ram_write, clock);

    always @(posedge clock or negedge reset) begin
      if (!reset) state = reset_cpu;
      else
            case (state)
                reset_cpu : begin 
                    PC = 0;
                    rom_address = PC;
                    AC = 0;
                    IR = 0;
                    rom_address = 0;
                    ram_address = 0;
                    ram_write = 0;
                    state = fetch;
                end
                fetch : begin
                    IR = rom_data_out;
                    PC = PC + 1;
                    state = decode;
                end
                decode : begin
                    rom_address = PC;
                    case (IR)
                        0 : begin state = execute_NOP; end
                        1 : begin state = execute_INC_A; end
                        2 : begin PC = PC + 1; state = execute_ADD_I; end
                        3 : begin PC = PC + 1; state = execute_ADD_M1; end
                        4 : begin PC = PC + 1; state = execute_MOV_A_TO_M1; end
                        5 : begin PC = PC + 1; state = execute_MOV_I_TO_A; end
                        6 : begin PC = PC + 1; state = execute_MOV_M_TO_A1; end
                        7 : begin PC = PC + 1; state = execute_JMP1; end
                        8 : begin PC = PC + 1; state = execute_JZ1; end
                    endcase
                end
                execute_NOP : begin  
                    rom_address = PC;
                    state = fetch;
                end
                execute_INC_A : begin
                    AC = AC + 1;
                    rom_address = PC;
                    state = fetch;
                end
                execute_ADD_I : begin
                    AC = AC + rom_data_out;
                    rom_address = PC;
                    state = fetch;
                end
                execute_ADD_M1 : begin
                    rom_address = PC;
                    ram_address = rom_data_out;
                    state = execute_ADD_M2;
                end
                execute_ADD_M2 : begin
                    AC = AC + ram_data_out;
                    rom_address = PC;
                    state = fetch;
                end
                execute_MOV_A_TO_M1  : begin
                    ram_write = 1;
                    ram_address = rom_data_out;
                    ram_data_in = AC;
                    state = execute_MOV_A_TO_M2;
                end
                execute_MOV_A_TO_M2  : begin
                    ram_write = 0;
                    rom_address = PC;
                    state = fetch;
                end
                execute_MOV_M_TO_A1  : begin
                    ram_write = 0;
                    ram_address = rom_data_out;
                    AC = ram_data_out;
                    state = execute_MOV_M_TO_A2;
                end
                execute_MOV_M_TO_A2  : begin
                    ram_write = 0;
                    rom_address = PC;
                    state = fetch;
                end
                execute_MOV_I_TO_A : begin
                    AC = rom_data_out;
                    rom_address = PC;
                    state = fetch;
                end
                execute_JMP1 : begin
                    PC = rom_data_out;
                    state = execute_JMP2;
                end
                execute_JMP2 : begin
                    rom_address = PC;
                    state = fetch;
                end
                execute_JZ1 : begin
                    if (AC == 0) begin 
                        PC = rom_data_out;
                        state = execute_JZ2;
                    end
                    else begin
                        rom_address = PC;
                        state = fetch;
                    end
                end
                execute_JZ2 : begin
                    rom_address = PC;
                    state = fetch;
                end
                default : state = fetch;
            endcase
    end
endmodule

module ram_memory(address, data_out, data_in, write, clock);

    input [7:0] address;
    output [7:0] data_out;
    input [7:0] data_in;
    input write, clock;

    reg [7:0] data_out;
    reg [7:0] memory [0:255];

    always @(posedge clock) begin 
        if (write) begin
            memory[address] = data_in;
        end
        else begin 
            data_out = memory[address];
        end
    end
endmodule

module rom_memory(address, data_out, clock);
    input [7:0] address;
    output [7:0] data_out;
    input clock;

    reg [7:0] data_out;

    always @(posedge clock) begin 
        case(address) 
        // 0 : data_out = 8'H05; // MOV D to A
        // 1 : data_out = 8'H2A; // INCA
        // 2 : data_out = 8'H04; // INCA
        // 3 : data_out = 8'H02; // NOP
        // 4 : data_out = 8'H03; // INCA
        // 5 : data_out = 8'H02;
        // 6 : data_out = 8'H00;

        0 : data_out = 8'H05; 
        1 : data_out = 8'H3A;
        2 : data_out = 8'H04;
        3 : data_out = 8'H02;
        4 : data_out = 8'H01;
        5 : data_out = 8'H06;
        6 : data_out = 8'H02;
        7 : data_out = 8'H00;
        // 6 : data_out = ;
        // 7 : data_out = ;
        // 8 : data_out = ;
        // 9 : data_out = ;
        default : data_out = 0;
        endcase
    end
endmodule