`timescale 1ns/1ns
`include "Example.v"

module Example_tb;
    
reg x1, x2, x3, x4;
wire f, g;

Example uut(f, g, x1, x2, x3, x4);

initial begin

    $dumpfile("Example.vcd");
    $dumpvars(0, Example_tb);
    x1 = 0;
    x2 = 0;
    x3 = 0;
    x4 = 0;
    #100;

    x1 = 0;
    x2 = 1;
    x3 = 1;
    x4 = 0;
    #100;

    $display("Example complete");

    $finish;
end

endmodule