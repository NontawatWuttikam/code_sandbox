module Example (f,g,x1,x2,x3,x4);

    input x1, x2, x3, x4;
    output f,g;
    wire x3_bar;
    wire x1x3_and, x2x3bar_and, x1x3bar_or, x3barx4_or;

    //logic
    and (x1x3_and, x1, x3);
    and (x2x3bar_and, x2, !x3);
    or (x1x3bar_or, x1, !x3);
    or (x3barx4_or, !x3, x4);

    or (f, x1x3_and, x2x3bar_and);
    and (g, x1x3bar_or, x3barx4_or);
     
endmodule