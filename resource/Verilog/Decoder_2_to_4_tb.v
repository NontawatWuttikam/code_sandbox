`timescale 1ns/1ns
`include "Decoder_2_to_4.v"

module Decoder_2_to_4_tb;
    
reg A1, A0;
wire [3:0] D;

reg clk;

Decoder_2_to_4 uut(D,A1,A0);

always #50 clk = !clk;

always begin


    #100;
    $display("%d : A1,A0 : %b  D : %d,%b",$time,{A1,A0},D,D);

end
initial begin

    $dumpfile("Decoder_2_to_4.vcd");
    $dumpvars(0, Decoder_2_to_4_tb);


    A1 = 0;
    A0 = 0;
    #100;

    A1 = 0;
    A0 = 1;
    #100;

    A1 = 1;
    A0 = 0;
    #100;

    A1 = 1;
    A0 = 1;
    #300;

    $display("Test complete");

    $finish;
end

endmodule