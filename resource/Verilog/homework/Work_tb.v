`timescale 1ns/1ns
`include "Work.v"
module Work_tb;
reg A, B, Ci;
wire S, Co;
Work uut(S, Co, A, B, Ci);
initial begin
    $dumpfile("Work_tb.vcd");
    $dumpvars(0, Work_tb);
    A = 0;
    B = 0;
    Ci = 0;
    #100;
    A = 0;
    B = 1;
    Ci = 1;
    #100;
    $display("Work complete");
    $finish;
end
endmodule