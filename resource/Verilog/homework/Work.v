module Work (S, Co, A, B, Ci);

    input A,B,Ci;
    output S,Co;
    wire AB_xor, ABxorCi_and, AB_and;

    //logic
    xor (AB_xor, A,B);
    and (ABxorCi_and, AB_xor, Ci);
    and(AB_and, A, B);

    xor (S, AB_xor, Ci);
    or (Co, ABxorCi_and, AB_and);
    
endmodule