module decoder_2_4(D0, D1, D2, D3, A, B);
    input A, B;
    output D0, D1, D2, D3;
    reg D0, D1, D2, D3;
    reg [1:0] inp;

    always @ (D0, D1, D2, D3, A, B)
    begin
      inp = {A,B};
      case(inp)
        2'b00: {D0, D1, D2, D3}= 4'b1000;
        2'b01: {D0, D1, D2, D3} = 4'b0100;
        2'b10: {D0, D1, D2, D3} = 4'b0010;
        2'b11: {D0, D1, D2, D3} = 4'b0001;
        default: {D0, D1, D2, D3} = 4'bx;
      endcase
    end
endmodule
    