`timescale 1ns/1ns
`include "decoder_2_4.v"

module decoder_2_4_tb;
    
    reg A, B;
    wire D0, D1, D2, D3;

decoder_2_4 uut(D0, D1, D2, D3, A, B);

initial begin

    $dumpfile("decoder_2_4_tb.vcd");
    $dumpvars(0, decoder_2_4_tb);

    {A,B} = 2'b00;
    #10
    {A,B} = 2'b01;
    #10
    {A,B} = 2'b10;
    #10
    {A,B} = 2'b11;
    #100

    $display("Work complete");

    $finish;
end

endmodule