module mux4_1 (Y, s1, s0, I0, I1, I2, I3);

    input s1, s0, I0, I1, I2, I3;
    output Y;

    assign Y = (s1 == 0 & s0 == 0)?I0 :
               (s1 == 0 & s0 == 1)?I1 :
               (s1 == 1 & s0 == 0)?I2 :
               (s1 == 1 & s0 == 1)?I3:1'bx;

endmodule