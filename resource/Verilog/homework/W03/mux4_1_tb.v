`timescale 1ns/1ns
`include "mux4_1.v"

module mux4_1_tb;
    
    reg s1, s0, I0, I1, I2, I3;
    wire Y;

mux4_1 uut(Y, s1, s0, I0, I1, I2, I3);

initial begin

    $dumpfile("mux4_1.vcd");
    $dumpvars(0, mux4_1_tb);


    s1 = 0;
    s0 = 0;
    I0 = 0;
    I1 = 0;
    I2 = 1;
    I3 = 0;
    #100;

    s1 = 1;
    s0 = 0;
    I0 = 0;
    I1 = 0;
    I2 = 1;
    I3 = 0;
    #100;

    $display("Work complete");

    $finish;
end

endmodule