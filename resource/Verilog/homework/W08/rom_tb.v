`timescale 1ns/1ns
`include "rom.v"
module rom_tb;

reg CLK, ENB, R;
reg [7:0] ADR;
wire [11:0] OUT;

always begin #50; CLK = !CLK; end 
rom uut(CLK, ENB, R, ADR, OUT);
initial begin

    $dumpfile("rom.vcd");
    $dumpvars(0, rom_tb);

    CLK = 0;
    #100
    ENB = 1;
    #100
    R = 1;
    ADR = 8'b00000000;
    #100
    ADR = 8'b00000001;
    #100
    ADR = 8'b00000010;
    #100
    ADR = 8'b00000011;
    #100
    ADR = 8'b00000100;
    #100
    ADR = 8'b00000101;
    #100
    ADR = 8'b00000110;
    #100
    ADR = 8'b00000111;
    #100
    ADR = 8'b00001000;
    #100
    ADR = 8'b00001001;
    #100

    $display("Work complete");
    $finish;
end
endmodule