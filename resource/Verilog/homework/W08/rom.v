module rom(CLK, ENB, R, ADR, OUT);

    input CLK, ENB, R;
    input [7:0] ADR;
    output [11:0] OUT;

    reg [11:0] OUT;
    reg [11:0] Mem [0:255];

    initial begin
        Mem[0] = 6;
        Mem[1] = 1;
        Mem[2] = 5;
        Mem[3] = 2;
        Mem[4] = 3;
        Mem[5] = 0;
        Mem[6] = 0;
        Mem[7] = 4;
        Mem[8] = 6;
        Mem[9] = 1;
    end

    always @(posedge CLK) begin
      if (ENB) begin
        if (R) begin
          OUT = Mem[ADR];
        end
      end
    end
endmodule

