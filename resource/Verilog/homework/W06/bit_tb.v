`timescale 1ns/1ns
`include "hw6.v"

module bit_tb;
    
reg  Load, Count, CLK, clr;
reg  [3:0] IN;
wire [3:0] A;
wire CU, CO;

hw6 uut(CLK,Load,Count,clr,IN,CO,A,CU);

always begin #50; CLK = !CLK; end
initial begin

    $dumpfile("bit_tb.vcd");
    $dumpvars(0, bit_tb);

    CLK = 0;
    clr = 1;
    #100;
    clr = 0;

    Load = 1;
    IN = 4'b0000;
    #50;
    Load = 0;
    Count = 1;
    #2000

    Count = 0;
    #2500

    Load = 1;
    IN = 4'b0110;
    #100
    Load = 0;


    $display("Work complete");

    $finish;
end

endmodule