module hw6(CLK,Load,Count,clr,IN,CO,A,CU);
input Load, Count, CLK, clr;
input [3:0] IN;
output CO;
output CU;
output [3:0] A;
reg [3:0] A;
always @(posedge CLK or posedge clr) begin
if (clr == 1'b1) A = 4'b0;

else if (Load == 1'b1) A = IN;

else if (Count == 1'b1) A = A + 1'b1;

else if (Count == 1'b0) A = A - 1'b1;

else A = A;
end
assign CO = Count & ~Load & (A == 4'b1111);
assign CU = ~Count & ~Load & (A == 4'b0000);

endmodule