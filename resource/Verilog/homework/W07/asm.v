module asm(S,CLK,RST,E,F,A);

input S, CLK, RST;
output E,F;
output [4:1] A;
reg  E, F;
reg[4:1] A;
reg[1:0] state;

parameter T0 = 2'b00, T1 = 2'b01, T2 = 2'b11;

always @ (posedge CLK or negedge RST)
begin
    if (RST == 0) state <= T0;
    else
    case (state)
        T0: if(S==1) state <= T1; else state <= T0;
        T1: if(A[3] & A[4]) state <= T2; else state <= T1;
        T2: state <= T0;
        default: state <= T0;
    endcase
end

always @ (posedge CLK)
begin
    case (state)
        T0: begin if(S)
                begin
                    A <= 4'b0000;
                    F <= 1'b0;
                end
        end
        T1:
            begin
                A <= A + 1'b1;
                if (A[3]) E <= 1'b1; else E <= 1'b0;
            end
        T2: F <= 1'b1;
    endcase
end
endmodule

​

​