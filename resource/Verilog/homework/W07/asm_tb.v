`timescale 1ns/1ns
`include "asm.v"
module asm_tb;
reg S, CLK, RST;
wire E, F, A;
asm uut(S,CLK,RST,E,F,A);
initial begin
    $dumpfile("asm.vcd");
    $dumpvars(0, asm_tb);
    RST = 0;
    S = 0;
    #100;
    RST = 1;
    #100;
    S = 1;
    #1000;
    $display("Work complete");
    $finish;
end
endmodule