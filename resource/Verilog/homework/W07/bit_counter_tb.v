`timescale 1ns/1ns
`include "bit_counter.v"

module bit_counter_tb;

reg CLK, RST,S;
reg [7:0] Data;
wire Done;
wire [3:0] B;
wire [7:0] A;
wire [1:0] state;

bit_counter uut(state,A,Data, S, CLK, RST, Done, B);

always #50 CLK = !CLK;

initial begin
  
  $dumpfile("bit_counter.vcd");
  $dumpvars(0, bit_counter_tb);
    CLK = 0;
    S = 0;
    #50;
    RST = 0;
    Data = 8'b01010111;

    S = 1;
    #1000;

    $display("finish");
    $finish;
end
endmodule