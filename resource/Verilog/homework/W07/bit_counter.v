module bit_counter(state,A,Data, S, CLK, RST, Done, B);

    input [7:0] Data;
    input CLK, RST, S;
    output Done;
    output [3:0] B;
    output [7:0] A;
    output [1:0] state;
    reg Done;
    reg [3:0] B;
    reg [7:0] A;
    reg [1:0] state;

    parameter s0 = 2'b00;
    parameter s1 = 2'b01; 
    parameter s2 = 2'b10;  

    always @ (posedge CLK or negedge RST)
    begin
      if (!RST) state <= s0;

      case(state)
        s0: begin
          A = 8'b00000000;
          B = 1'b0;
          if (S == 1'b0) A <= Data;
          A <= Data;
          Done = 1'b0;
          if(S == 1'b1) state <= s1;
        end
        s1: begin
          if (A[0] == 1'b1) B <= B + 1;
          A <= A >> 1;
          if (A == 8'b00000000) state <= s2;
          else state <= s1;
        end
        s2: begin
          if (S == 0) state <= s0; 
          else state <= s2;
          Done <= 1'b1;
        end
      endcase

    end
endmodule