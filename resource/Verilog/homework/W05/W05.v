module W05(z, x, CLK, RST);

    input x, CLK, RST;
    output z;
    reg z;
    reg[1:0] state;

    parameter s0 = 2'b00;
    parameter s1 = 2'b01;
    parameter s2 = 2'b10;
    parameter s3 = 2'b11;

    always @(posedge CLK or negedge RST)
    begin

      if (!RST) state = s0;
      
      case(state)
        s0: begin 
                if (x==0) state = s2;
                else if (x==1) state = s1;
            end
        s1: begin  
                if (x==0) state = s3;
                else if (x==1) state = s0;
            end
        s2: begin  
                if (x==0) state = s0;
                else if (x==1) state = s3;
            end
        s3: begin  
                if (x==0) state = s1;
                else if (x==1) state = s2;
            end
        default : state = s0;

      endcase 

      z = (state==s0)? 1'b0 :
          (state==s1)? 1'b1 :
          (state==s2)? 1'b1 :
          (state==s3) ? 1'b0 : 1'bx;
    end

endmodule