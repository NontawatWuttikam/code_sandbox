`timescale 1ns/ 1ns
`include "W05.v"

module W05_tb;

    reg x,CLK,RST;
    wire z;

W05 uut(z,x,CLK,RST);

always begin
    #50 CLK = !CLK ;
end

initial begin

    $dumpfile("W05.vcd");
    $dumpvars(0, W05_tb);

    CLK = 1;
    RST = 0;
    #100;
    RST = 1;
    x = 1;
    #100;
    x = 0;
    #100;
    x=1;
    #100;
    x=1;
    #100;
    x=1;
    #100
    x=0;
    #100


    $display("Test Complete");
    $finish;

end

endmodule