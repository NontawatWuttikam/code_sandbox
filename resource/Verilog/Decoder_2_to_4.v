module Decoder_2_to_4 (D,A1,A0);

    output [3:0] D;
    input A1, A0;
    reg [3:0] D;

    always @ (A1,A0)
    begin
        
        if ({A1,A0} == 2'B00) D = 4'b0001;
        else if ({A1,A0} == 2'B01) D = 4'b0010;
        else if ({A1,A0} == 2'B10) D = 4'b0100;
        else if ({A1,A0} == 2'B11) D = 4'b1000;
        else D = 4'bxxxx;

    end

endmodule