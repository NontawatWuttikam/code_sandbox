//**********************************************************
//*  Test file for Testing API service **Challenge 2020**  *
//*  1.Run this file                                       *
//*  2.If exit with no output error                        *
//*    then all test cases are passed                      *
//*  3.test failed will be show in the debugging console   *
//*                                                        *
//********************************************************** */
async function main() {
    console.log("**********************************************************\n"+
    "*  Test file for Testing API service **Challenge 2020**  *\n"+
    "*  1.Run this file                                       *\n"+
    "*  2.If exit with no output error                        *\n"+
    "*    then all test cases are passed                      *\n"+
    "*  3.test failed will be show in the debugging console   *\n"+
    "*                                                        *\n"+
    "********************************************************** ")
    axios = require('axios')
    assert = require('assert')
    const s0lved = [
        "0f443379ff3295a3c9c0664efc9e01e49fa06d6f628bb684b739140f41144ae0",
        "0fc0fc3d0c2c42930e6ffdffc413e36b885c41123b4778ae2ebe9d432247217f",
        "0c6a082670c2da69d6a01416d7b18e9aa5d1fa857bcc47cd2289efcac07e9cb9",
        "240d509e42f3a91a1c5b977864d58cd855478b9de5e4632aa79e039702ccf2e8",
        "a9f0bd27c1190885ccc784c8b8cca598e9b3fbb94b55d13969b16885fd3eb191",
        "91752551fe72c05eb4919a1beb077efaf8bf5d193e95d880b32ec7c9c4457cd9",
        "df63996ecf590fa5b71ea1d33e782b84315c8482be38591802ea971081a9f627",
        "f09d0c137502c9722083b121a0c9ab55665c09edeb79f760cd52c0ff55bc2a00",
        "742ad586086cd47eb86d83b73f474f578afd445458bc95b092a4c5edd967c2e9",
        "1f68ca07b93e4f19f40c8f60ff2082d7161f6d214d1735e0f76aafefc86fb1fb",
        "0742c665ee58a92dffd4b7ca9f7bffbaf591fb9dd5c27e9ff6a26214cbbaa875",
        "ee18ec086448798cab99fc26b6d805789ca1e76a6852aadc7ec18005647ae347",
        "f16e32ea026f57066da95692495f41eed682ac383c6624a15d933515d0e4a618",
        "15c52751fd5808ac1f3eb045deb0a6f5119dbf37969b71c3a8358ced76ed1b56",
        "00abd16a0103a68b908a0bdb1cfabedcf755638c6f90efc03ee66ea94c101d31",
        "a1b9d54e32cbb665b941f37ec8d1114b0a9e5a5e2ef4f888b1ccae979a72bbd6",
        "36e2c56261476ee5b7afc71785a1ca1b0215e1c8a41d589bf7469d4cc21e5f9b",
        "fb0bd737a06269222e34998e6cd3fe3c5669f2d0e2e1126b55cf2b10ecc4de38",
        "7c170302c43efea4026da3a67c048f420c5ccd216d234e38e26406d881cef460",
        "56fcbff5fc99c051a0a5723c674a5b6d6696731c4f4679e4b34a3bc810aeafce",
        "b602b445514a7d6a72343d8e50ad88d66b2e3fbe55f6396fd49016d6887cf52b",
        "2d7c510c868243f881f657d988e33edaa4924968167b90483969646f1bdc827c",
        "8c7bd7c72ef44e4e14f4ae728470190a9eefb07d6d26dd2baca7524a23211bd0",
        "225bfbdf1192ef61b75d09022d087ec4c8f163c17564bb13d84ab0b845bb2414",
        "d13ec4367571687dc941ea0a4cb0744a2a5c162d74ec6f3449f1413146b61d82"
    ]

    uri = "http://127.0.0.1:5000/"
    const TEAM_AMOUNT = 25 //24+admin
    const TEXT_LENGTH = 500000
    const CORRECT_RESPONSE_TEXT = "It's corrected"
    const INCORRECT_RESPONSE_TEXT = "Incorrect"

    let promise01 = axios.post(uri+"/auth",{
        username: "team01",
        password: "5ZyaBS6cqJ9Mhwen"
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise02 = axios.post(uri+"/auth",{
        username: "team02",
        password: "CxwTACt8ExgYpAzc"
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise03 = axios.post(uri+"/auth",{
        username: "team03",
        password: 'aMXQL8u4acNs4Q4D'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise04 = axios.post(uri+"/auth",{
        username: "team04",
        password: 'GKmXJVfU4dCmjkq9'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise05 = axios.post(uri+"/auth",{
        username: "team05",
        password: 'j3D9XHPkX4qPf4FG'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise06 = axios.post(uri+"/auth",{
        username: "team06",
        password: "5JEjGNR6qKjaQALB"
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise07 = axios.post(uri+"/auth",{
        username: "team07",
        password: 'Yg5qjsYKFMyCVAst'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise08 = axios.post(uri+"/auth",{
        username: "team08",
        password: 'uDqGyLLtv8BjHL8d'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise09 = axios.post(uri+"/auth",{
        username: "team09",
        password: "MTvhsZwFJXX6t8R7"
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})
    let promise10 = axios.post(uri+"/auth",{
        username: "team10",
        password: 'nXDzafLr6PQwRLyX'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})
    let promise11 = axios.post(uri+"/auth",{
        username: "team11",
        password: 'zWsAgaAB9KNnxxHu'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise12 = axios.post(uri+"/auth",{
        username: "team12",
        password: '8W3688p7dkNg2SJW'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise13 = axios.post(uri+"/auth",{
        username: "team13",
        password: '8sxP4SnZRDstVSau'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise14 = axios.post(uri+"/auth",{
        username: "team14",
        password: 'TyTXze47uMMUwqnx'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise15 = axios.post(uri+"/auth",{
        username: "team15",
        password: '5DLdyWDQUw79ZPCm'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise16 = axios.post(uri+"/auth",{
        username: "team16",
        password: 'BDmTN48ZSVu3PKBw'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise17 = axios.post(uri+"/auth",{
        username: "team17",
        password: 'YGZYXp4F8xW3S2m7'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise18 = axios.post(uri+"/auth",{
        username: "team18",
        password: 'PFpNU8SKMJmvqG74'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise19 = axios.post(uri+"/auth",{
        username: "team19",
        password: 'pgftjcQpXyp5Ct6M'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise20 = axios.post(uri+"/auth",{
        username: "team20",
        password: 'SuDNh87TNBV2SHqr'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise21 = axios.post(uri+"/auth",{
        username: "team21",
        password: 'WvmG5KAACKG8C7y2'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise22 = axios.post(uri+"/auth",{
        username: "team22",
        password: 'Qmhwurj5pgS98VNF'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise23 = axios.post(uri+"/auth",{
        username: "team23",
        password: 'yFsWGzqrZWPHGMd5'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promise24 = axios.post(uri+"/auth",{
        username: "team24",
        password: 'XaVS2tsKVx7hdjv8'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let promiseADMIN = axios.post(uri+"/auth",{
        username: "admin",
        password: 'L1qudT3ns10nExper1m3n7'
    }).catch(err=>{console.log("error code : "+err.response.status+" "+err.response.statusText)})

    let jsonToken01 = await promise01
    let jsonToken02 = await promise02
    let jsonToken03 = await promise03
    let jsonToken04 = await promise04
    let jsonToken05 = await promise05
    let jsonToken06 = await promise06
    let jsonToken07 = await promise07
    let jsonToken08 = await promise08
    let jsonToken09 = await promise09
    let jsonToken10 = await promise10
    let jsonToken11 = await promise11
    let jsonToken12 = await promise12
    let jsonToken13 = await promise13
    let jsonToken14 = await promise14
    let jsonToken15 = await promise15
    let jsonToken16 = await promise16
    let jsonToken17 = await promise17
    let jsonToken18 = await promise18
    let jsonToken19 = await promise19
    let jsonToken20 = await promise20
    let jsonToken21 = await promise21
    let jsonToken22 = await promise22
    let jsonToken23 = await promise23
    let jsonToken24 = await promise24
    let jsonTokenADMIN = await promiseADMIN

    assert(jsonToken02.status == 200,"Get Token from 2 user failed")
    assert(jsonToken01.status == 200,"Get Token from 1 user failed")
    assert(jsonToken03.status == 200,"Get Token from 3 user failed")
    assert(jsonToken04.status == 200,"Get Token from 4 user failed")
    assert(jsonToken05.status == 200,"Get Token from 5 user failed")
    assert(jsonToken06.status == 200,"Get Token from 6 user failed")
    assert(jsonToken07.status == 200,"Get Token from 7 user failed")
    assert(jsonToken08.status == 200,"Get Token from 8 user failed")
    assert(jsonToken09.status == 200,"Get Token from 9 user failed")
    assert(jsonToken10.status == 200,"Get Token from 10 user failed")
    assert(jsonToken11.status == 200,"Get Token from 11 user failed")
    assert(jsonToken12.status == 200,"Get Token from 12 user failed")
    assert(jsonToken13.status == 200,"Get Token from 13 user failed")
    assert(jsonToken15.status == 200,"Get Token from 15 user failed")
    assert(jsonToken16.status == 200,"Get Token from 16 user failed")
    assert(jsonToken17.status == 200,"Get Token from 17 user failed")
    assert(jsonToken18.status == 200,"Get Token from 18 user failed")
    assert(jsonToken19.status == 200,"Get Token from 19 user failed")
    assert(jsonToken20.status == 200,"Get Token from 20 user failed")
    assert(jsonToken21.status == 200,"Get Token from 21 user failed")
    assert(jsonToken22.status == 200,"Get Token from 22 user failed")
    assert(jsonToken23.status == 200,"Get Token from 23 user failed")
    assert(jsonToken24.status == 200,"Get Token from 24 user failed")
    assert(jsonTokenADMIN.status == 200,"Get Token from ADMIN user failed")
    assert(jsonToken14.status == 200,"Get Token from 14 user failed")

    var authHeader = []
    authHeader[1] = {
        headers: {
            Authorization : "JWT " + jsonToken01.data.access_token
        }
    }
    authHeader[2] = {
        headers: {
            Authorization : "JWT " + jsonToken02.data.access_token
        }
    }
    authHeader[3] = {
        headers: {
            Authorization : "JWT " + jsonToken03.data.access_token
        }
    }
    authHeader[4] = {
        headers: {
            Authorization : "JWT " + jsonToken04.data.access_token
        }
    }
    authHeader[5] = {
        headers: {
            Authorization : "JWT " + jsonToken05.data.access_token
        }
    }
    authHeader[6] = {
        headers: {
            Authorization : "JWT " + jsonToken06.data.access_token
        }
    }
    authHeader[7] = {
        headers: {
            Authorization : "JWT " + jsonToken07.data.access_token
        }
    }
    authHeader[8] = {
        headers: {
            Authorization : "JWT " + jsonToken08.data.access_token
        }
    }
    authHeader[9] = {
        headers: {
            Authorization : "JWT " + jsonToken09.data.access_token
        }
    }
    authHeader[10] = {
        headers: {
            Authorization : "JWT " + jsonToken10.data.access_token
        }
    }
    authHeader[11] = {
        headers: {
            Authorization : "JWT " + jsonToken11.data.access_token
        }
    }
    authHeader[12] = {
        headers: {
            Authorization : "JWT " + jsonToken12.data.access_token
        }
    }
    authHeader[13] = {
        headers: {
            Authorization : "JWT " + jsonToken13.data.access_token
        }
    }
    authHeader[14] = {
        headers: {
            Authorization : "JWT " + jsonToken14.data.access_token
        }
    }
    authHeader[15] = {
        headers: {
            Authorization : "JWT " + jsonToken15.data.access_token
        }
    }
    authHeader[17] = {
        headers: {
            Authorization : "JWT " + jsonToken17.data.access_token
        }
    }
    authHeader[18] = {
        headers: {
            Authorization : "JWT " + jsonToken18.data.access_token
        }
    }
    authHeader[19] = {
        headers: {
            Authorization : "JWT " + jsonToken19.data.access_token
        }
    }
    authHeader[20] = {
        headers: {
            Authorization : "JWT " + jsonToken20.data.access_token
        }
    }
    authHeader[21] = {
        headers: {
            Authorization : "JWT " + jsonToken21.data.access_token
        }
    }
    authHeader[22] = {
        headers: {
            Authorization : "JWT " + jsonToken22.data.access_token
        }
    }
    authHeader[23] = {
        headers: {
            Authorization : "JWT " + jsonToken23.data.access_token
        }
    }
    authHeader[24] = {
        headers: {
            Authorization : "JWT " + jsonToken24.data.access_token
        }
    }
    authHeader[25] = {
        headers: {
            Authorization : "JWT " + jsonTokenADMIN.data.access_token
        }
    }
    authHeader[16] = {
        headers: {
            Authorization : "JWT " + jsonToken16.data.access_token
        }
    }

    //get text file
    for(var i = 1 ; i<=TEAM_AMOUNT ; i++) {
        let num = i
        axios.get(uri+"/data",authHeader[i]
        ).then(res=> {
            assert(res.data.length == TEXT_LENGTH,"get text file failed team : ")
        })
    }

    // submit successfully
    for(var i = 1 ; i<=TEAM_AMOUNT ; i++) {
        let num = i
        axios.post(uri+"/score?chksum="+s0lved[i-1],{},authHeader[i]
        ).catch(err=> {
            assert(err.response.data.msg==CORRECT_RESPONSE_TEXT,"submit corrected answer failed team : "+num,)
        })
    }

    //submit incorrect successfully
    for(var i = 1 ; i<=TEAM_AMOUNT ; i++) {
        let num = i
        axios.post(uri+"/score?chksum="+"rand0mha3h",{},authHeader[i]
        ).catch(err=> {
            assert(err.response.data.msg==INCORRECT_RESPONSE_TEXT,"submit incorrect answer failed team : "+num,)
        })
    }

    // /info check score ==100
    for(var i = 1 ; i<=TEAM_AMOUNT ; i++) {
        let num = i
        axios.get(uri+"/info",authHeader[i]
        ).then(res=> {
            assert(res.data.score == 100,"check score failed team : "+num)
        })
    }

    // /api_spec test
    axios.get(uri+"/spec").then(res=> {
        assert(res.status == 200,"api spec request failed")
    })
}
main()

