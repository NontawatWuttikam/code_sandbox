import numpy as np
import matplotlib.pyplot as plt
import cv2

def downsampling2(src, ks = 2):
    dimg = np.zeros(shape = (int(np.floor((src.shape[0]/ks))), int(np.floor((src.shape[1]/ks))), 3), dtype= np.uint8)
    incrementer = 0
    dimgcount = 0
    while (incrementer < (src.shape[0] / ks == 0 and src.shape[0] or src.shape[0]-1)):
        incrementer = incrementer + ks
        for i,j in zip(range(dimg.shape[1]), range(0, src.shape[1],ks)):
            dimg[dimgcount,i] = src[incrementer-ks:incrementer, j:j+ks,:][0][0]
            # print(str(i) + ' '+ str(j))
        dimgcount += 1
    
    return dimg

img = cv2.imread('D:\\code_sandbox\\code_sandbox\\resource\\Image Processing\\gon.png')
cap = cv2.VideoCapture(0)
print(img.shape)
print(downsampling2(img).shape)
cv2.imwrite(img = downsampling2(downsampling2(img,2)), filename = "gonex.png")