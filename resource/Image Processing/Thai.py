import numpy as np
import matplotlib.pyplot as plt

Thai = np.zeros((6,9,3),dtype= np.uint8)
Thai[:,:] = 255
Thai[:1,:] = [255,0,0]
Thai[-1,:] = [255,0,0]
Thai[2:4,:] = [0,0,255]
plt.imshow(Thai)
plt.show()