import cv2
import numpy as np
cap = cv2.VideoCapture(0)
while True:
    _, frame = cap.read()
    R = frame[:,:,2]
    G = frame[:,:,1]
    B = frame[:,:,0]
    roi = (G>30) & (B>4) & (R >= 0) & (G<120) & (B<50) & (R < 40)
    roi = np.logical_not(roi)
    frame2 = frame.copy()
    frame2[roi] = 0
    cv2.imshow('frame2', frame2)
    cv2.imshow('frame', frame)
    cv2.waitKey(1)