import cv2
import random
cap = cv2.VideoCapture(0)
_, frame1 = cap.read()
hisframe = [frame1]
while True :
    k = random.choice(range(0,len(hisframe),1))
    color = random.choice([0,1,2])
    _, frame = cap.read()
    frame[:,:,color] = 0 
    hisframe.append(frame)
    cv2.imshow('frame', hisframe[k])
    if(len(hisframe) > 20):
        hisframe = hisframe[1:len(hisframe)-1]
    cv2.waitKey(1)
