import numpy as np
import matplotlib.pyplot as plt
import cv2

def transform(x):
    y = x.copy()
    mask1 = (x >= 0) & (x < 180)
    mask2 = x >=180
    y[mask1] = 4/9 * x[mask1]
    y[mask2] = 7/3 * x[mask2]
    return y

def replace_pixel(x, cf):
    y = x.copy()
    for i in range (255):
        y[x==i] = cf[i]
    return y

A = cv2.imread('D:\\code_sandbox\\code_sandbox\\resource\\Image Processing\\pic.png')
AR = A[:,:,2]
AG = A[:,:,1]
AB = A[:,:,0]


HR = plt.hist(AR.ravel(), bins=256)[0]
cfr = np.cumsum(HR) / (AR.shape[0] * AR.shape[1]) * 255
cfr = np.round(cfr)

HG = plt.hist(AG.ravel(), bins=256)[0]
cfg = np.cumsum(HG) / (AG.shape[0] * AG.shape[1]) * 255
cfg = np.round(cfg)

HB = plt.hist(AB.ravel(), bins=256)[0]
cfb = np.cumsum(HB) / (AB.shape[0] * AB.shape[1]) * 255
cfb = np.round(cfb)

R = replace_pixel(AR, cfr)
G = replace_pixel(AG, cfg)
B = replace_pixel(AB, cfb)
plt.show()

img = cv2.merge((B,G,R))
plt.subplot(1,2,1)
plt.imshow(A)
plt.subplot(1,2,2)
plt.imshow(transform(cv2.cvtColor(A, cv2.COLOR_BGR2GRAY))*(-1), cmap = 'gray')
plt.show()