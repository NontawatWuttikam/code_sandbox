import cv2
import matplotlib.pyplot as plt

_, imageA = cv2.VideoCapture(0).read()
img = cv2.cvtColor(imageA, cv2.COLOR_BGR2HSV)
plt.subplot(2,2,1)
plt.imshow(imageA[:,:,::-1])
plt.subplot(2,2,2)
plt.imshow(img[:,:,0], cmap = 'gray')
plt.subplot(2,2,3)
plt.imshow(img[:,:,1], cmap = 'gray')
plt.subplot(2,2,4)
plt.imshow(img[:,:,2], cmap = 'gray')
plt.show()