import numpy as np
import matplotlib.pyplot as plt

R = np.zeros((256,256,3), dtype = np.uint8);
G = np.zeros((256,256,3), dtype = np.uint8);
B = np.zeros((256,256,3), dtype = np.uint8);
Gr =np.zeros((256,256), dtype = np.uint8);
for i in range(R.shape[0]):
    Gr[:,i] = i
    R[:,i] = [i,0,0]
    G[:,i] = [0,i,0]
    B[:,i] = [0,0,i]
plt.subplot(141)
plt.imshow(R)
plt.subplot(142)
plt.imshow(G)
plt.subplot(143)
plt.imshow(B)
plt.subplot(144)
plt.imshow(Gr, cmap = 'gray')
plt.show()