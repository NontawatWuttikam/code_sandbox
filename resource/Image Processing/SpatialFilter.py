import numpy as np
import matplotlib.pyplot as plt
import cv2

def pad (A, n = 2, dtype = np.uint8):
    B = np.zeros((A.shape[0] + (2*n), A.shape[1] + (2*n), ), dtype = dtype)
    B[n:-n, n:-n] = A
    return B

def mirror_pad2 (A, n = 2, dtype = np.uint8):
    B = np.zeros((A.shape[0] + (2*n), A.shape[1] + (2*n), ), dtype = dtype)
    counterY = np.ceil(n / A.shape[1])
    counterX = np.ceil(n / A.shape[0])
    invUD = A[:,::-1]
    invLR = A[::-1,:]

    for i in range(counterY):
        
    print (counterX)
    print (counterY)
    return B

def mirror_pad (A, n = 2, dtype = np.uint8):
    return np.pad(A,n,mode='symmetric')

def circular_pad (A, n = 2, dtype = np.uint8):
    return np.pad(A,n,mode='wrap')

np.random.seed(1)
A = np.random.randint(0,15,(5,5))
print(mirror_pad2(A))