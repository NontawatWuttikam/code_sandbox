import numpy as np
import matplotlib.pyplot as plt

n = 600
r = 300

y,x = np.ogrid[-300:300, -300:300]
mask = x*x + y*y <= r*r #<=90,0000

redcircle = np.full((n, n,3),fill_value= 255)
redcircle[mask] = [188,0,45]

japan = np.full((1000,1500,3), fill_value=255, dtype=np.uint8)
japan[200:800,450:1050] = redcircle
plt.imshow(japan)
plt.show()