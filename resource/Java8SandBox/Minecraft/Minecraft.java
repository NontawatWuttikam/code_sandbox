public class Minecraft {

}

// class WoodenSword {

//     double AttackDamage() {
//         return 20.5;
//     }

// }

// class IronSword {

//     double AttackDamage() {
//         return 100.5;
//     }
    
// }

// class DiamondSword {

//     double AttackDamage() {
//         return 300;
//     }

// }

interface Attackable {

    double attackDamage();

}

abstract class Weapon implements Attackable{
    public abstract double attackDamage();
}

abstract class Monster implements Attackable{

    public abstract double attackDamage();

}

abstract class HarmfulBlock implements Attackable{

    public abstract double attackDamage();

}


abstract class Sword extends Weapon {
    abstract double attackDamage();
}

class WoodenSword extends Sword {

	@Override
    public double attackDamage() {
        return 20.5;
    }
}

class IronSword extends Sword {
    @Override
	public double attackDamage() {
		return 70;
	}
}

class Zombie {

    public double attackDamage() {
		return 70;
    }
    
}

class Creeper {

    public double attackDamage() {
		return 70;
    }
    
}

class Lava {

    public double attackDamage() {
		return 70;
    }
    
}

class Steve {
    double Hp = 500;

    void gotAttackedBy(WoodenSword sword) {
        Hp  = Hp - sword.attackDamage();
    }
    void gotAttackedBy(IronSword sword) {
        Hp  = Hp - sword.attackDamage();
    }
    void gotAttackedBy(Lava lava) {
        Hp  = Hp - lava.attackDamage();
    }
    void gotAttackedBy(Creeper creeper) {
        Hp  = Hp - creeper.attackDamage();
    }
    void gotAttackedBy(Zombie zombie) {
        Hp  = Hp - zombie.attackDamage();
    }

}

