package Java8SandBox;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Java8Sim {
    public static void main(String[] args) {
        List<Integer> list = new Arrayz<Integer>().toList
        (new Sorter<Integer>() 
        {
			@Override
			public void sort(List<Integer> b) {
				Collections.sort(b);
            }},1,7,5,3,5,33,6,4,62,6,3,77,3,88,34,5);
        StringFormatter<String> formatter = (str)-> {
            return str.toUpperCase();
        };
    List<Character> characters = Arrays.asList('A','B','C');
    List<Integer> below20 = list.stream().filter(x->{return x<20;}).collect(Collectors.toList());
    Optional<Integer> sum = Optional.of(list.stream().mapToInt(o->o).sum());
    System.out.println(formatter.format("abc"));
    below20.forEach(System.out::print);
    }
}
@SuppressWarnings("varargs")
class Arrayz<T> {    
    public List<T> toList(Sorter<T> s,T... a) {
        List<T> b = Arrays.asList(a);
        s.sort(b);
        return b;
    }
}
interface Sorter<T> {
    void sort(List<T> b);
}
interface StringFormatter<T> {
    T format(T str);
}
