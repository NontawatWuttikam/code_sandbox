package javaz;

import java.awt.Color;
import java.awt.Dimension;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.awt.Graphics;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.color.*;

public class Maze {

    // public static String MAZE_ARRAY[] =       {"###########F#################",
    //                                             "#.....#.#.....#............#",
    //                                             "#######.##..#.#.....####...#",
    //                                             "#E...##.###...#.....#....###",
    //                                             "###.###.###########.########",
    //                                             "##...........#...........###",
    //                                             "####.######..#.########..###",
    //                                             "###..##.#.#..#....#......###",
    //                                             "###.###.........####.....###",
    //                                             "###.###.........####.....###",
    //                                             "###.###.........####.....###",
    //                                             "###.###.........####.....###",
    //                                             "###.###.........####.....###",
    //                                             "############################"};
    public static String MAZE_ARRAY[] = {"##################################",
                                         "#E.......#.........#..#...#......#",
                                         "#.....#..######...##..#.###.####.#",
                                         "#.....#.........#.....#..#....#..#",
                                         "#####.###############.#.##...##..#",
                                         "#..........#.....#..#...####.#...#",
                                         "#.########.#.#.###.##.#..#...###.#",
                                         "#...#..#.#.#.#........#.##.###...#",
                                         "#.#.#..#.....##########..........#",
                                         "###.####.#.#...#......###.#####..#",
                                         "#........#.#.######.#...#......#.#",
                                         "#.###..#.#.#...#....#.#.#####..#.#",
                                         "#.#....#.#.##.#########.#...#.####",
                                         "###.####.#....#.......#.....#....#",
                                         "#.....#.########..############.###",
                                         "#..####..#.......................#",
                                         "#......#.#.......................#",
                                         "#........#.......................#",
                                         "#........#.......................#",
                                         "#........#.......................#",
                                         "#........#.......................#",
                                         "#........#.......................#",
                                         "#........#.......................#",
                                         "#####################F############"

    };
    private static final Integer DELAY = 50;
    private static Point findPosition(char cpoint) {
        int count=0; Point point = new Point(0,0);
        for(String str : MAZE_ARRAY) {
            for(int i = 0 ; i< str.length();i++) 
                if(str.charAt(i)==cpoint) {
                    point.setX(i);
                    point.setY(count);
                }
            count++;
        }
        return point;
    }
    private static String[] maze2 = MAZE_ARRAY.clone();
    private static String[] render(Point currentPosition) {
        int count = 0;
        String[] temp = new String[MAZE_ARRAY.length];
        for(String str : MAZE_ARRAY) {
            for(int i = 0 ;i< str.length();i++) {
                if(currentPosition.getY()==count&currentPosition.getX()==i) {
                    char[] arStr = str.toCharArray();
                    arStr[i] = Point.START_POINT;
                    str = String.valueOf(arStr);
                    MAZE_ARRAY[count]  =  str;
                    temp[count] = str;
                }
                else  {
                    char[] arStr = str.toCharArray();
                    arStr[i] = maze2[count].charAt(i);
                    str = String.valueOf(arStr);
                    MAZE_ARRAY[count] = str;
                    temp[count] = str;

                }
            }
            count++;
        }
        for(String i : MAZE_ARRAY)
            System.out.println(i);
            System.out.println("CurrentPosition X : "+currentPosition.getX()+"  Y : "+currentPosition.getY()+"\n\n");
            return temp;
    }
    private static boolean isWalked(Point current){
        for(Point x : discoverdPath)
            if(current.isEquals(x))
                return true;
        return false;
    }
    private static JFrame frame = new JFrame("My Maze");
    private static void addPanel(JPanel jpanel) {
        frame.add(jpanel);
    }
    private static final Point START_POINT = findPosition(Point.START_POINT);
    private static final Point FINISH_POINT = findPosition(Point.FINISH_POINT);
    private static Stack<Point> walkStack = new Stack<>();
    private static List<Point> reverseStack = new ArrayList<>();
    private static List<Point> discoverdPath = new ArrayList<>(); 
    public static void main(String[] args) throws InterruptedException {
        Point currentPoint = START_POINT;
        boolean deadEnd;
        frame.setSize(new Dimension(600,400));
        frame.setVisible(true);
        String labelText = "<html>";
        JLabel screen = new JLabel();
        GridsCanvas gridsCanvas = new GridsCanvas(10, 10, MAZE_ARRAY.length, 10, MAZE_ARRAY,discoverdPath);
        frame.add(gridsCanvas);
        frame.add(screen);
        while(true) { 
            deadEnd = true;
            //checkleft
            Point left = new Point(currentPoint.getX()-1,currentPoint.getY());
            if((MAZE_ARRAY[currentPoint.getY()].charAt(currentPoint.getX()-1)==Point.WALK_PATH|
            MAZE_ARRAY[currentPoint.getY()].charAt(currentPoint.getX()-1)==Point.FINISH_POINT)&&
            !isWalked(new Point(currentPoint.getX()-1,currentPoint.getY()))){
                walkStack.push(new Point(currentPoint.getX()-1,currentPoint.getY()));

                discoverdPath.add(new Point(currentPoint.getX()-1,currentPoint.getY()));
                deadEnd=false;
            }
            //check right
            Point right = new Point(currentPoint.getX()+1,currentPoint.getY());
            if((MAZE_ARRAY[currentPoint.getY()].charAt(currentPoint.getX()+1)==Point.WALK_PATH|
            MAZE_ARRAY[currentPoint.getY()].charAt(currentPoint.getX()+1)==Point.FINISH_POINT)&&
            !isWalked(new Point(currentPoint.getX()+1,currentPoint.getY()))){
                walkStack.push(new Point(currentPoint.getX()+1,currentPoint.getY())); 
                discoverdPath.add(new Point(currentPoint.getX()+1,currentPoint.getY()));
                deadEnd=false;
            }
            //check down
            Point down = new Point(currentPoint.getX(),currentPoint.getY()+1);
            if((MAZE_ARRAY[currentPoint.getY()+1].charAt(currentPoint.getX())==Point.WALK_PATH|
            MAZE_ARRAY[currentPoint.getY()+1].charAt(currentPoint.getX())==Point.FINISH_POINT)&&
            !isWalked(new Point(currentPoint.getX(),currentPoint.getY()+1))){
                walkStack.push(new Point(currentPoint.getX(),currentPoint.getY()+1)); 
                discoverdPath.add(new Point(currentPoint.getX(),currentPoint.getY()+1));
                deadEnd=false;
            }
            //check up
            Point up = new Point(currentPoint.getX(),currentPoint.getY()-1);
            if((MAZE_ARRAY[currentPoint.getY()-1].charAt(currentPoint.getX())==Point.WALK_PATH|
            MAZE_ARRAY[currentPoint.getY()-1].charAt(currentPoint.getX())==Point.FINISH_POINT)&&
            !isWalked(new Point(currentPoint.getX(),currentPoint.getY()-1))){
                walkStack.push(new Point(currentPoint.getX(),currentPoint.getY()-1)); 
                discoverdPath.add(new Point(currentPoint.getX(),currentPoint.getY()-1));
                deadEnd=false;
            }
            reverseStack.add(currentPoint);
            currentPoint = walkStack.pop();
            if(deadEnd) {
                Stack<Point> temp = new Stack<>();
                temp.addAll(reverseStack);
                while(true) {
                    addPanel(new GridsCanvas(10, 10, MAZE_ARRAY.length, 10,  render(temp.pop()),discoverdPath));
                    screen.setText(labelText);  
                    SwingUtilities.updateComponentTreeUI(frame);
                    Thread.sleep(DELAY);
                    if((temp.peek().getX()-1==currentPoint.getX()&temp.peek().getY()==currentPoint.getY())|
                    (temp.peek().getX()+1==currentPoint.getX()&temp.peek().getY()==currentPoint.getY())|
                    (temp.peek().getX()==currentPoint.getX()&temp.peek().getY()-1==currentPoint.getY())|
                    (temp.peek().getX()==currentPoint.getX()&temp.peek().getY()+1==currentPoint.getY())) {
                        addPanel(new GridsCanvas(10, 10, MAZE_ARRAY.length, 10,  render(temp.pop()),discoverdPath));
                        SwingUtilities.updateComponentTreeUI(frame);
                        // Thread.sleep(DELAY);
                        break;}
                }
                reverseStack.clear();
                reverseStack.addAll(temp);
                temp.clear();

            }
            System.out.flush();  
            GridsCanvas gridsCanvasx = new GridsCanvas(10, 10, MAZE_ARRAY.length, 10, MAZE_ARRAY,discoverdPath);
            addPanel(gridsCanvasx);
            render(currentPoint);
            if(currentPoint.isEquals(FINISH_POINT))
                break;

            //Render to window\
            int count = 0;
        for(String str : MAZE_ARRAY) {
            for(int i = 0 ;i< str.length();i++) {
                if(currentPoint.getY()==count&currentPoint.getX()==i) {
                    char[] arStr = str.toCharArray();
                    arStr[i] = Point.START_POINT;
                    str = String.valueOf(arStr);
                    MAZE_ARRAY[count] = str;
                }
                else  {
                    char[] arStr = str.toCharArray();
                    arStr[i] = maze2[count].charAt(i);
                    str = String.valueOf(arStr);
                    MAZE_ARRAY[count] = str;
                }
            }
            count++;
        }

        for(String i : MAZE_ARRAY) {
            labelText += i + "<br>";
        }
            labelText+="</html>";
            screen.setText(labelText);    
            Thread.sleep(DELAY);
        }
        System.out.println("AI winnnn!!");
    

}
    
}


class Point {
    private int x;
    private int y;
    private boolean isDiscovered = false;
    public static final char START_POINT = 'E';
    public static final char WALK_PATH = '.';
    public static final char WALL = '#';
    public static final char FINISH_POINT = 'F';
    public Point(int x,int y) {
        this.setX(x);
        this.setY(y);
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getX() {
        return this.x;
    }
    public int getY() {
        return this.y;
    }
    public boolean isDiscovered() {
        return this.isDiscovered;
    }
    public void setDiscovered(boolean bool) {
        isDiscovered=bool;
    }
    public boolean isEquals(Point p) {
        if(this.getX()==p.getX()&&this.getY()==p.getY()) return true;
        return false;
    }

    
}

class GridsCanvas extends JPanel {
    int width, height;
  
    int rows;
  
    int cols;

    String[] MAZE_ARRAY;

    List<Point> discoveredPath;
  
    GridsCanvas(int w, int h, int r, int c,String[] MAZE_ARRAY,List<Point> discoveredPath) {
      setSize(width = w, height = h);
      rows = r;
      cols = c;
      this.MAZE_ARRAY = MAZE_ARRAY;
      this.discoveredPath = discoveredPath;
    }
  
    @Override
    public void paint(Graphics g) {
      int i;
      width = getSize().width;
      height = getSize().height;
  
      // draw the rows
      int sumx = 0;
      int sumy = 0;
      int count = 0;
      for(String j : MAZE_ARRAY) {
          for(int p = 0 ; p<j.length();p++) {
            if(j.charAt(p)==Point.WALK_PATH)
            g.setColor(Color.WHITE);
            Point temp = new Point(p,count);
            // if(discoveredPath.contains(temp))
            for(Point dis : discoveredPath)
                if(dis.isEquals(temp))
                    g.setColor(Color.PINK);
              if(j.charAt(p)==Point.FINISH_POINT)
                g.setColor(Color.RED);
                if(j.charAt(p)==Point.START_POINT)
                g.setColor(Color.BLUE);
                if(j.charAt(p)==Point.WALL)
                g.setColor(Color.BLACK);
                g.fillRect(sumx, sumy, 20, 20);
                sumx+=20;
          }
          count++;
          sumx=0;
          sumy+=20;
      }
      
    }
  }

