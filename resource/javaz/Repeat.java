public class Repeat {
    public static void main(String[] args) {
        String str = new Repeater().repeat("Hello").repeat("world").repeat("555").setUpperCase().build();
        String str2 = new Repeater().repeat("Hello").repeat("world").repeat("555").setUpperCase().build();
        System.out.println(str);
    } 
}

class Repeater {
    private static String text="";
    public Repeater(String texts) {
        text=texts;
    }
    public Repeater() {

    }
    public  Repeater repeat(String repeater) {
        text+=repeater+" ";
        return new Repeater(text);
    }
    public String build() {
        String text2=text;
        text="";
        return text2;
    }
    public Repeater setUpperCase() {
        text = text.toUpperCase();
        return new Repeater();
    }

}