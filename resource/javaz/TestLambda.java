import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;
import java.util.concurrent.CopyOnWriteArrayList;

public class TestLambda {
    public static void main(String[] args) {
    List<Queue<Integer>> queue = new ArrayList<>(100);
    for(int g = 0;g<10;g++)
        queue.add(new LinkedList<>());
    List<Integer> listNumber = new CopyOnWriteArrayList<>();
    int[] numArray = {12,54,235,984,1,475,305,8,4,55,6,2345787,7058,374,12};
    int max=0;
    for(int i : numArray) {
        listNumber.add(i);
        max = (i>max)? i:max;
    }
    for(int digitPosition = 0;digitPosition< String.valueOf(max).length();digitPosition++) {
        for(Integer i : listNumber) {
            queue.get((i/((int)Math.pow(10,digitPosition)))%10).add(i); 
            listNumber.remove(Integer.valueOf(i));
        }
        for(Queue<Integer> que : queue)
            while(que.size()!=0)
                listNumber.add(que.poll());
    }
    for(int result : listNumber)
        System.out.print(result+" ");
}
}

