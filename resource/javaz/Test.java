import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Test {
    public static void main(String[] args) throws ParseException {
    String input = "20200105";
    String format = "yyyyMMdd";
    
    SimpleDateFormat df = new SimpleDateFormat(format);
    Date date = df.parse(input);

    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    int week = cal.get(Calendar.WEEK_OF_YEAR);
    System.out.println(week);
    System.out.println(new SimpleDateFormat("w").format(date));
    }
}