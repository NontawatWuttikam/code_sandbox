import numpy as np
import sounddevice as sd
import threading
KICK = 70.20
SILENT = 0;
DO3 = 130.81
RE3 = 146.83
ME3 = 164.81
FA3 = 174.61
SOL3 = 196.00 
LA3 = 220.00
SE3 = 246.94
DO4 =  261.63
RE4 = 293.66
ME4 = 329.63
FA4 = 349.23
SOL4 = 392.00
LA4 = 440.00
SE4 = 493.88
DO5 = 523.33
RE5 = 587.33
ME5 = 659.25
FA5 = 698.46
SOL5 = 783.99
LA5 = 880.00
SE5 = 987.77
def generate_Sine_Note(frequency,time):
    # Generate time of samples between 0 and two seconds
    samples = np.arange(44100 * time) / 44100.0
    # Recall that a sinusoidal wave of frequency f has formula w(t) = A*sin(2*pi*f*t)
    wave = 10000 * np.sin(2 * np.pi * frequency * samples)
    # Convert it to wav format (16 bits)
    wav_wave = np.array(wave, dtype=np.int16)
    return wav_wave

def get_Doraemon_Lead(speed):
    sol4ft = generate_Sine_Note(SOL4,2*speed/3)
    do5t = generate_Sine_Note(DO5,speed/3)
    do5ft = generate_Sine_Note(DO5,2*speed/3)
    me5t = generate_Sine_Note(ME5,speed/3)
    la5ft = generate_Sine_Note(LA5,2*speed/3)
    sol5f = generate_Sine_Note(SOL5,speed)
    sol5ft = generate_Sine_Note(SOL5,2*speed/3)
    la5t = generate_Sine_Note(LA5,speed/3)
    fa5ft = generate_Sine_Note(FA5,2*speed/3)
    re5f = generate_Sine_Note(RE5,speed)
    la4ft = generate_Sine_Note(LA4,2*speed/3)
    re5t = generate_Sine_Note(RE5,speed/3)
    re5ft = generate_Sine_Note(RE5,2*speed/3)
    fa5t = generate_Sine_Note(FA5,speed/3)
    se5ft = generate_Sine_Note(SE5,2*speed/3)
    se5t = generate_Sine_Note(SE5,speed/3)
    sol5t = generate_Sine_Note(SOL5,speed/3)
    me5ft = generate_Sine_Note(ME5,2*speed/3)
    se4f = generate_Sine_Note(SE4,speed)
    re5L8 = generate_Sine_Note(RE5,4*speed)
    return np.concatenate((sol4ft,do5t,do5ft,me5t,la5ft,me5t,sol5f,
                            sol5ft,la5t,sol5ft,me5t,fa5ft,me5t,re5f,
                            la4ft,re5t,re5ft,fa5t,se5ft,se5t,la5ft,sol5t,
                            fa5ft,fa5t,me5ft,re5t,la4ft,se4f,do5t,re5L8))

def play_doraemon_Lead(speed):
    sd.play(get_Doraemon_Lead(speed),blocking=True)

doraemon = threading.Thread(target=play_doraemon_Lead,args=(0.5,))
doraemon.run()
def get_AW_Lead(speed):
    do = generate_Sine_Note(DO5,speed);
    me = generate_Sine_Note(ME5,speed);
    la =generate_Sine_Note(LA5,speed);
    sol = generate_Sine_Note(SOL5,speed);
    se = generate_Sine_Note(SE4,speed);
    return np.concatenate((do,do,do,me,la,la,la,sol,me,me,me,me,se,se,se,se),axis = 0)
def get_AW_BassLine(speed):
    la =generate_Sine_Note(LA3,speed);
    fa =generate_Sine_Note(FA3,speed);
    do = generate_Sine_Note(DO3,speed);
    sol = generate_Sine_Note(SOL3,speed);
    return np.concatenate((la,la,la,la,fa,fa,fa,fa,do,do,do,do,sol,sol,sol,sol),axis = 0)
def get_Kick(speed):
    k = generate_Sine_Note(KICK,speed)*3;
    s = generate_Sine_Note(SILENT,0.1)
    kick = np.concatenate((k,s,s,s,s,s,s),axis=0)
    return np.concatenate((kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick,kick),axis=0)
def play_Kick():
    while(1):
        sd.play(get_Kick(0.1),blocking = True)
def play_AW_Lead(speed):
    while(1):
        sd.play(get_AW_Lead(speed),blocking = True)
def play_AW_BassLine(speed):
    while(1):
        sd.play(get_AW_BassLine(speed),blocking = True)

play_Lead = threading.Thread(target=play_AW_Lead,args=(0.7,))
play_BassLine = threading.Thread(target=play_AW_BassLine,args=(0.7,))
play_Kick = threading.Thread(target=play_Kick)


play_Kick.start()
play_Lead.start()
play_BassLine.start()