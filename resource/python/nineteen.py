def countPossibleWord(text):
    t = count_char(text,'t')
    i = count_char(text,'i')
    base = min([t,i])
    while 1:
        if base == 0:
            return 0
        e = count_char(text,'e')
        n = count_char(text,'n')
        if not e >= base * 3:
            base = base - 1
            continue
        elif not n >= (base*2) + 1:
            base = base - 1
            continue
        else :
            return base


def count_char(strin,ch):
    count = 0
    for i in strin:
        if i == ch:
            count += 1
    return count
            
inp = input()
print(countPossibleWord(inp))
    