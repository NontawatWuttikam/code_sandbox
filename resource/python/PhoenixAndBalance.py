import math
testcase = int(input())

ans = []
for i in range(testcase):
    num = int(input())
    arr = []
    for i in range(1,num+1,1):
        arr.append(math.pow(2,i))
    left = arr.pop()
    right = arr.pop()
    a = len(arr)/2

    left = left + sum(arr[:int(len(arr)/2)])
    right = right + sum(arr[int(len(arr)/2):len(arr)])

    ans.append(abs(left-right))

for i in ans:
    print(int(i))
