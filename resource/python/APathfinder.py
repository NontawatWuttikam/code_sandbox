import numpy as np
import matplotlib.pyplot as plt
import tkinter as tk
import math
from random import randint
DISCOVERED = 2
WAKED = 3
PATH = 5
class node:
    def __init__(self,f,g,h,iswall,cord):
        self.f = f
        self.g = g
        self.h = h
        self.cord = cord
        self.iswall = 0
map_array = np.zeros((50,50),dtype=np.uint8)
def render(arr,flag):
    image = np.zeros((*map_array.shape,3),dtype=np.uint8)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if arr[i,j] == 0:
                image[i,j] = [255,255,255]
            elif arr[i,j] == DISCOVERED:
                image[i,j] = [255,200,200]
            elif arr[i,j] == WAKED:
                image[i,j] = [200,200,255]
            elif arr[i,j] == 8:
                image[i,j] = [0,0,255]
            elif arr[i,j] == 9:
                image[i,j] = [0,255,0]
            elif arr[i,j] == PATH:
                image[i,j] = [100,100,255]
    plt.imshow(image)
    plt.pause(0.000001)
    if flag:
        return image
    plt.cla()
    return None

node_map = map_array.tolist()
for i in range(len(node_map)):
    for j in range(len(node_map[i])):
        node_map[i][j] = node(-1,-1,-1,0,[i,j])
        if map_array[i][j] == 1:
            node_map[i][j].iswall = 1
currentPos = [13,19]
map_array[13,19] = 8
rand1 = randint(map_array.shape[0]-int(2*map_array.shape[0]/3),map_array.shape[0]-1)
rand2 = randint(map_array.shape[1]-int(2*map_array.shape[1]/3),map_array.shape[1]-1)
end_node = [49,20]
map_array[49,20] = 9
for i in range(1000):
    rand1 = randint(0,map_array.shape[0]-1)
    rand2 = randint(0,map_array.shape[1]-1)
    if((rand1 == currentPos[0] and rand2 == currentPos[1]) or (rand1 == end_node[0] and rand2 == end_node[1])):
        continue
    map_array[rand1,rand2] = 1
    node_map[rand1][rand2].iswall = 1
discovered = []
priority_list = []
trace_stack = []
walk_stack = []
image = None
def run(current):
    global node_map
    global map_array
    global end_node
    global trace_stack
    global priority_list
    global image
    global walk_stack
    nowalk = 1
    flag = 0
    print(current)
    if(current[0] == end_node[0] and current[1] == end_node[1]):
        print("dd")
        for j in trace_stack:
            map_array[j[0],j[1]] = PATH
        flag=1
        image = render(map_array,flag)
        return
    trace_stack.append(current)
    walk_stack.append(current)
    if(current is not currentPos):
        map_array[current[0],current[1]] = WAKED
    up = [current[0]-1,current[1]]
    down = [current[0]+1,current[1]]
    left = [current[0],current[1]-1]
    right = [current[0],current[1]+1]
    upleft = [current[0]-1,current[1]-1]
    upright = [current[0]-1,current[1]+1]
    downleft = [current[0]+1,current[1]-1]
    downright = [current[0]+1,current[1]+1]

    if not is_out_of_bound(node_map,up) :
        if node_map[up[0]][up[1]].iswall == 0 and not is_discovered(up):
            node_map[up[0]][up[1]].g = math.sqrt(math.pow(up[0]-current[0],2)+math.pow(up[1]-current[1],2))
            node_map[up[0]][up[1]].h = math.sqrt(math.pow(up[0]-end_node[0],2)+math.pow(up[1]-end_node[1],2))
            node_map[up[0]][up[1]].f = node_map[up[0]][up[1]].g + node_map[up[0]][up[1]].h
            priority_list.append(node_map[up[0]][up[1]])
            if up is not end_node :
                map_array[up[0],up[1]] = DISCOVERED
            discovered.append(up)
            nowalk = 0
    # if not is_out_of_bound(node_map,upleft) :
    #     if node_map[upleft[0]][upleft[1]].iswall == 0 and not is_discovered(upleft):
    #         node_map[upleft[0]][upleft[1]].g = math.sqrt(math.pow(upleft[0]-current[0],2)+math.pow(upleft[1]-current[1],2))
    #         node_map[upleft[0]][upleft[1]].h = math.sqrt(math.pow(upleft[0]-end_node[0],2)+math.pow(upleft[1]-end_node[1],2))
    #         node_map[upleft[0]][upleft[1]].f = node_map[upleft[0]][upleft[1]].g + node_map[upleft[0]][upleft[1]].h
    #         priority_list.append(node_map[upleft[0]][upleft[1]])
    #         if upleft is not end_node :
    #             map_array[upleft[0],upleft[1]] = DISCOVERED
    #         discovered.append(upleft)
    # if not is_out_of_bound(node_map,upright) :
    #     if node_map[upright[0]][upright[1]].iswall == 0 and not is_discovered(upright):
    #         node_map[upright[0]][upright[1]].g = math.sqrt(math.pow(upright[0]-current[0],2)+math.pow(upright[1]-current[1],2))
    #         node_map[upright[0]][upright[1]].h = math.sqrt(math.pow(upright[0]-end_node[0],2)+math.pow(upright[1]-end_node[1],2))
    #         node_map[upright[0]][upright[1]].f = node_map[upright[0]][upright[1]].g + node_map[upright[0]][upright[1]].h
    #         priority_list.append(node_map[upright[0]][upright[1]])
    #         if upright is not end_node :
    #             map_array[upright[0],upright[1]] = DISCOVERED
    #         discovered.append(upright)
    if not is_out_of_bound(node_map,down) :
        if node_map[down[0]][down[1]].iswall == 0 and not is_discovered(down):
            node_map[down[0]][down[1]].g = math.sqrt(math.pow(down[0]-current[0],2)+math.pow(down[1]-current[1],2))
            node_map[down[0]][down[1]].h = math.sqrt(math.pow(down[0]-end_node[0],2)+math.pow(down[1]-end_node[1],2))
            node_map[down[0]][down[1]].f = node_map[down[0]][down[1]].g + node_map[down[0]][down[1]].h
            priority_list.append(node_map[down[0]][down[1]])
            if down is not end_node :
                map_array[down[0],down[1]] = DISCOVERED
            discovered.append(down)
            nowalk = 0
    # if not is_out_of_bound(node_map,downleft) :
    #     if node_map[downleft[0]][downleft[1]].iswall == 0 and not is_discovered(downleft):
    #         node_map[downleft[0]][downleft[1]].g = math.sqrt(math.pow(downleft[0]-current[0],2)+math.pow(downleft[1]-current[1],2))
    #         node_map[downleft[0]][downleft[1]].h = math.sqrt(math.pow(downleft[0]-end_node[0],2)+math.pow(downleft[1]-end_node[1],2))
    #         node_map[downleft[0]][downleft[1]].f = node_map[downleft[0]][downleft[1]].g + node_map[downleft[0]][downleft[1]].h
    #         priority_list.append(node_map[downleft[0]][downleft[1]])
    #         if downleft is not end_node :
    #             map_array[downleft[0],downleft[1]] = DISCOVERED
    #         discovered.append(downleft)
    # if not is_out_of_bound(node_map,downright) :
    #     if node_map[downright[0]][downright[1]].iswall == 0 and not is_discovered(downright):
    #         node_map[downright[0]][downright[1]].g = math.sqrt(math.pow(downright[0]-current[0],2)+math.pow(downright[1]-current[1],2))
    #         node_map[downright[0]][downright[1]].h = math.sqrt(math.pow(downright[0]-end_node[0],2)+math.pow(downright[1]-end_node[1],2))
    #         node_map[downright[0]][downright[1]].f = node_map[downright[0]][downright[1]].g + node_map[downright[0]][downright[1]].h
    #         priority_list.append(node_map[downright[0]][downright[1]])
    #         if downright is not end_node:
    #             map_array[downright[0],downright[1]] = DISCOVERED
    #         discovered.append(downright)
    if not is_out_of_bound(node_map,right) :
        if node_map[right[0]][right[1]].iswall == 0 and not is_discovered(right):
            node_map[right[0]][right[1]].g = math.sqrt(math.pow(right[0]-current[0],2)+math.pow(right[1]-current[1],2))
            node_map[right[0]][right[1]].h = math.sqrt(math.pow(right[0]-end_node[0],2)+math.pow(right[1]-end_node[1],2))
            node_map[right[0]][right[1]].f = node_map[right[0]][right[1]].g + node_map[right[0]][right[1]].h
            priority_list.append(node_map[right[0]][right[1]])
            if right is not end_node:
                map_array[right[0],right[1]] = DISCOVERED
            discovered.append(right)
            nowalk = 0
    if not is_out_of_bound(node_map,left) :
        if node_map[left[0]][left[1]].iswall == 0 and not is_discovered(left):
            node_map[left[0]][left[1]].g = math.sqrt(math.pow(left[0]-current[0],2)+math.pow(left[1]-current[1],2))
            node_map[left[0]][left[1]].h = math.sqrt(math.pow(left[0]-end_node[0],2)+math.pow(left[1]-end_node[1],2))
            node_map[left[0]][left[1]].f = node_map[left[0]][left[1]].g + node_map[left[0]][left[1]].h
            priority_list.append(node_map[left[0]][left[1]])
            if left is not end_node:
                map_array[left[0],left[1]] = DISCOVERED
            discovered.append(left)
            nowalk = 0
    if nowalk:
        for i in walk_stack:
            trace_stack.remove(i)
        walk_stack.clear()
    position_to_walk = []
    for i in priority_list:
        cur = min(priority_list,key = lambda x:x.f)
        priority_list.remove(cur)
        position_to_walk.append(cur)
        if(len(priority_list)==0):
            break
        if(cur.f != priority_list[0]):
            break
    for i in position_to_walk:
        render(map_array,flag)
        pos = min(position_to_walk,key = lambda x:x.f)
        position_to_walk.remove(min(position_to_walk,key = lambda x:x.f))
        run(pos.cord)

def is_out_of_bound(arr,pos):
    global currentPos
    if(pos[0]==len(arr)):
        return 1
    if(pos[1]==len(arr[pos[0]])):
        return 1
    if(currentPos == pos):
        return 1
    return 0

def is_discovered(pos):
    global discovered
    if pos in discovered:
        return 1
    return 0
run(currentPos)
plt.imshow(image)
plt.show()
# render(map_array)
# image = np.zeros((*map_array.shape,3),dtype=np.uint8)
# image[0,0] = (1,1,1)

