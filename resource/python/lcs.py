def fibo(n):
   if(n == 1):
      return 1
   if(n == 0):
      return 0
   else:
      return fibo(n-1)+fibo(n-2)

def plus(n):
   if(n==1):
      return 1
   else:
      return n+plus(n-1)

def toarray(n):
   if(n==1):
      ar = [1]
      return ar
   else:
      arr = []
      arr.append(n)
      return arr + toarray(n-1);

def lcm(lis,multiplier):
   flag=0
   for i in lis:
      if i%2 != 0:
         flag = 1
      else:
         flag = 0
         break
   if not flag:
      temp = []
      for i in lis:
         if i%2 == 0:
            i = i/2;
            temp.append(i)
         else:
            temp.append(i)
      return lcm(temp,multiplier*2)
   elif flag:
      result = 1;
      for i in lis:
         result = result*i
      return result * multiplier;

def charset(charzet,st,k,norm):
      if(k==0):
         print(st)
         return;
      for i in charzet:
         new = st+i;
         charset(charzet,new,k-1,norm);

def positivetonegative(origin,num):
   print(num)
   if(num!= -origin):
      positivetonegative(origin,num-1)
   positivetonegative(origin,num+1)

def star(num,amount):
   if(num==amount):
      return
   print("*",end="")
   star(num+1,amount)

def printstar(line,increaser):
   star(0,increaser)
   print("\n")
   if(increaser==line):
      return
   printstar(line,increaser+1);

# print(toarray(10))
# print(lcm([10,5,2],1))
# print(plus(100))
# print(fibo(5))
# print(charset(['b','o','a','t'],"",4,4))
print(printstar(4,0))