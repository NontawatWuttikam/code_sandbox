import string
import sys
import hashlib
class Node(object):
    def __init__(self, letter,freq,left,right):
        self.letter = letter
        self.left = left
        self.right = right
        self.freq = freq
    def get_freq(self):
        return self.freq
    def get_letter(self):
        return self.letter
    def leftNode(self):
        return self.left
    def rightNode(self):
        return self.right
letter_list = {i:0 for i in string.punctuation+string.whitespace+string.ascii_letters+'1234567890'}
text = open("C:\\Users\\nttuser\\Desktop\\Axios sandbox\\resource\\python\\textfile.txt","r").read()
Node_list=[]
binary_string = ""
for i in text:
    if i in letter_list:
        letter_list[i]+=1

for i in letter_list:
    if (letter_list[i] != 0):
        Node_list.append(Node(i,letter_list[i],None,None))

Node_list.sort(key = lambda x: (x.get_freq(),ord(x.get_letter())))
for i in Node_list:
    print(i.get_letter() + " " + str(i.get_freq()))
while(len(Node_list)>1):
    temp_node1 = Node_list.pop(0)
    temp_node2 = Node_list.pop(0)
    if(temp_node1.get_freq()>temp_node2.get_freq()):
        Node_list.append(Node(None,temp_node1.get_freq()+
            temp_node2.get_freq(),temp_node2,temp_node1))
    elif(temp_node1.get_freq()<=temp_node2.get_freq()):
        Node_list.append(Node(None,temp_node1.get_freq()+
            temp_node2.get_freq(),temp_node1,temp_node2))
    Node_list.sort(key = lambda x: x.get_freq())

freq_list = {i:-1 for i in string.punctuation+string.whitespace+string.ascii_letters+'1234567890'}
# def traversal(node,bina):
#     global Node_list
#     global freq_list
#     if node.get_letter() is None :
#         if node.leftNode != None :
#             bina += "0"
#         elif node.right != None:
#             bina += "1"
#         traversal(node.right,bina)
#         traversal(node.left,bina)
#     else:
#         if(bina==""):
#             freq_list[node.get_letter()] = '0'
#         else:
#             freq_list[node.get_letter()] = bina
#             bina=""
#         #return [bina,Node.get_letter()]

def make_code(node, prefix):
    if node is None:
        return []
    if node.letter is not None:
        return [(prefix, node.letter)]
    else:
        result = []
        result.extend(make_code(node.left, prefix + '0'))
        result.extend(make_code(node.right, prefix + '1'))
        return result

if(len(Node_list)==0):
    print("Empty string!")
    exit()
result2 = make_code(Node_list[0],"")
result2.sort(key = lambda x:int(x[0],base=2))
dictionary = {i[1]:i[0] for i in result2}
print(dictionary)
result=""
for c in text:
    result += str(dictionary[c])
print(result)
# dig = hashlib.sha256(result.encode('utf-8')).hexdigest()
# print(dig)
f = open("digested.bin","wb")
b = bytearray()
b.extend(map(ord,result))
f.write(bytes(result,'utf-8'))
f.close()
