import numpy as np;
import matplotlib.pyplot as plt 

DISCOVERED = 2
WAKED = 3
class node:
    def __init__(self,f,g,h,iswall,cord):
        self.f = f
        self.g = g
        self.h = h
        self.cord = cord
        self.iswall = 0
map_array = np.array([[0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,9,1,1,1,1,0,0],
                      [0,0,1,1,1,0,0,0,0,0],
                      [0,0,1,0,1,0,0,0,0,0],
                      [0,0,1,0,1,0,0,0,0,0],
                      [0,0,1,8,1,1,1,1,0,0],
                      [0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0]])
def render(arr,flag):
    image = np.zeros((*map_array.shape,3),dtype=np.uint8)
    for i in range(arr.shape[0]):
        for j in range(arr.shape[1]):
            if arr[i,j] == 0:
                image[i,j] = [255,255,255]
            elif arr[i,j] == DISCOVERED:
                image[i,j] = [255,200,200]
            elif arr[i,j] == WAKED:
                image[i,j] = [200,200,255]
            elif arr[i,j] == 8:
                image[i,j] = [0,0,255]
            elif arr[i,j] == 9:
                image[i,j] = [0,255,0]
    plt.imshow(image)
    plt.pause(0.1)
    if not flag:
        plt.cla()

openlist = []
closelist = []
start = node(0,0,0,0,)
