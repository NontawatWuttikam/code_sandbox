import numpy as np

ar3 = np.array([[[1,2,3],[4,5,6]],[[1,2,3],[4,5,6]]])
ar2 = np.array([[1,2,3],[4,5,6],[7,8,9]])
class matrix:
    def __init__(self,data):
        self.data = np.array(data)
        self.column_size = self.data.shape[1]
        self.row_size = self.data.shape[0]
    def transpose(self):
        transposed = np.zeros((self.column_size,self.row_size))
        for i in range(self.row_size):
            for j in range(self.column_size):
                transposed[j,i] = self.data[i,j]
        self.data = transposed
        self.row_size,self.column_size = self.column_size,self.row_size;

matr = matrix([[1,2,3],[4,5,6]])
print(matr.data[:,:])
matr.transpose()
print(matr.data[:,:])

