import numpy as np
from matplotlib import pyplot as plt
from asyncio.tasks import sleep

array = np.array([list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         list("0000000000"),
         ])

def render(array):
    image = np.zeros((*array.shape, 3), dtype = np.uint8)
    image[array == '0'] = (0,0,0)
    image[array == 'O'] = (255,255,0)
    image[array == 'I'] = (255,0,255)

    plt.imshow(image)
    plt.axis('off')
    plt.pause(0.5)
    plt.cla()

class O_Block:
    def __init__(self,indx,indy):
        self.indx = indx
        self.indy = indy
        self.DIMENSION = [2,2]
        self.LETTER = 'O'
        self.pos = [[indx,indy],[indx+1,indy],[indx,indy+1],[indx+1,indy+1]]

class I_Block:
    def __init__(self,indx,indy):
        self.indx = indx
        self.indy = indy
        self.DIMENSION = [1,4]
        self.LETTER = 'I'
        self.pos = [[indx,indy],[indx+1,indy],[indx+2,indy],[indx+3,indy]]



render_array = array.copy()
blockloop = 1
while 1 :
    i=1
    if blockloop % 4 == 2:
        block = O_Block(1,1)
    elif blockloop % 4 == 1:
        block = I_Block(1,1)
    while 1:
        render_array[block.pos[0][0]+i,block.pos[0][1]] = block.LETTER
        render_array[block.pos[1][0]+i,block.pos[1][1]] = block.LETTER
        render_array[block.pos[2][0]+i,block.pos[2][1]] = block.LETTER
        render_array[block.pos[3][0]+i,block.pos[3][1]] = block.LETTER
        render(render_array)
        #HIT GROUND
        if i+block.pos[3][0] ==19:
            print("HIT GROUND")
            array = render_array.copy()
            break
        render_array = array.copy()
        i += 1
        sleep(100)
    blockloop+=1

