criteria = input()
criteria = [int(x) for x in criteria.split(" ")]

scores = input()
scores = [int(x) for x in scores.split(" ")]

def func(x):
    if x >= scores[criteria[1]-1] and x != 0:
        return 1
    return 0
print(len(list(filter(func,scores))))