class node:
    def __init__(self,data,neightbor,isVisited):
        self.data = data
        self.neightbor = neightbor
        self.isVisited = isVisited;

nodeA = node("A",[],0)
nodeB = node("B",[],0)
nodeC = node("C",[],0)
nodeD = node("D",[],0)
nodeE = node("E",[],0)
nodeF = node("F",[],0)
nodeG = node("G",[],0)
nodeH = node("H",[],0)
nodeS = node("S",[],0)

nodeA.neightbor.extend([nodeB,nodeS])
nodeB.neightbor.extend([nodeA])
nodeC.neightbor.extend([nodeD,nodeE,nodeF,nodeS])
nodeD.neightbor.extend([nodeC])
nodeE.neightbor.extend([nodeC,nodeH])
nodeF.neightbor.extend([nodeC,nodeG])
nodeG.neightbor.extend([nodeS,nodeF,nodeH])
nodeH.neightbor.extend([nodeE,nodeG])
nodeS.neightbor.extend([nodeA,nodeC,nodeG])

def depth_first_search(startNode):
    temp = startNode
    stack = []
    string = ""
    if(startNode == None):
        return
    stack.append(temp)
    while 1:
        current = stack[len(stack)-1]
        if not current.isVisited:
            string = string + current.data
            current.isVisited = 1
        flag = 0
        for i in range(len(current.neightbor)):
            if not current.neightbor[i].isVisited:
                # current.neightbor.remove(current.neightbor[i])
                current = current.neightbor[i]
                stack.append(current)
                flag = 1
                break
        if not flag:
            stack.pop()
        if(len(stack)==0):
            break
    return string

print(depth_first_search(nodeA))
        