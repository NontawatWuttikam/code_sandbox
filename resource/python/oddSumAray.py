def oddSum(arr,origin,flag,i,j):
    if sum(arr) % 2 != 0:
        print("YES")
        return
    else:
        if(i==len(arr)):
            i=0;
            j+=1
        if(j==len(arr)):
            print('NO')
            return
        ars = origin.copy()
        ars[j] = ars[(i)%len(ars)]
        oddSum(ars,origin,flag+1,i+1,j)

# def sumArr(arr):
#     i=0
#     j=1
#     if(len(arr)==0):
#         print("NO")
#         return
#     if(len(arr)==1):
#         if(arr[0] % 2 == 0):
#             print("NO")
#         else:
#             print("YES");
#     while(sum(arr)% 2 == 0 ):
#         if(i==len(arr)):
#             i=0;
#             j+=1
#         if(j==len(arr)):
#             print('NO')
#             return
#         ars = arr.copy()
#         ars[i] = ars[(j)%len(ars)]
#         i = i+1
#     print("YES")

test_case = input()
arrayset =[]
for i in range(int(test_case)):
    arr_len = input()
    arr_str = input()
    array = arr_str.split(" ")
    length = len(array)
    for i in range(length):
        if(not array[i].isdigit()):
            del array[i]
    lis = [int(i) for i in array]
    arrayset.append(lis)

for a in arrayset:
    oddSum(a,a,1,0,1)
