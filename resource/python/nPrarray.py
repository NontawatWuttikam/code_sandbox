import sys
import math
import random
import time
arr = [1,55,63,78,1,4,67,32]
def nPr(i,j):
    return math.factorial(i)/math.factorial(i-j)
def find_max_nPr(arr):
    maxx = -1
    result = []
    for i in range(len(arr)):
        for j in range(len(arr)):
            if(arr[j]>arr[i]) or i==j:
                continue
            npr = nPr(arr[i],arr[j])
            if npr > maxx:
                maxx = npr
                result = [arr[i],arr[j]]
    return result

def efficient_nPr(arr):
    maxx1 = max(arr)
    arr.remove(maxx1)
    maxx2 = max(arr)
    return [maxx1,maxx2]
print(find_max_nPr(arr))
print(efficient_nPr(arr))
            
        
