import time
import numpy as np
import matplotlib.pyplot as plt
from asyncio.tasks import sleep
import math


lisx = range(10000)
lisy =[]
for i in lisx:
    pos = 0
    for j in range(1000):
        pos = pos + math.sin(i*j)
    lisy.append(pos)

plt.plot(lisy)
plt.show()