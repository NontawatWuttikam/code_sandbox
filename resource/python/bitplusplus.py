num = int(input())
count = 0

for i in range(num):
    statement = input()
    if ("X" in statement or "x" in statement) and "++" in statement:
        count += 1
    if ("X" in statement or "x" in statement) and "--" in statement:
        count -= 1
    else:
        continue

print(count)
