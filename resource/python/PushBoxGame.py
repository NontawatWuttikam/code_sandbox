import matplotlib.pyplot as plt
import numpy as np
import cv2
import tkinter as tk
from pynput.keyboard import Key,Listener
def keydown(e):
    global currentPosition
    print("down")
    keydown_action()
    print(currentPosition)

def keyup(e):
    global currentPosition
    print("up")
    keyup_action()
    print(currentPosition)

def keyleft(e):
    global currentPosition
    print("left")
    keyleft_action()
    print(currentPosition)

def keyright(e):
    global currentPosition
    print("right")
    keyright_action()
    print(currentPosition)

x=y=0
map_array = [[0,0,0,0,0,0,0,5,5],
             [0,1,1,1,1,1,0,0,0],
             [0,1,1,1,1,1,1,1,0],
             [0,0,0,0,1,0,0,1,0],
             [5,5,0,1,1,1,1,1,0],
             [5,5,0,1,1,0,0,0,0],
             [5,5,0,1,1,0,5,5,5],
             [5,5,0,0,0,0,5,5,5]]
currentPosition = [2,3]
boxes= [[2,4],[2,5]]
win = [[2,6],[2,7]]
def render():
    x=0;y=0
    xx=0;yy=0
    foundbox=0;
    for arr_x in map_array:
        xx=0
        for arr_y in arr_x:
            if(arr_y==0):
                canvas.create_rectangle(x,y,x+50,y+50,fill="black")
                x+=50
            if(arr_y==1):
                if(xx==currentPosition[1] and yy==currentPosition[0]):
                    canvas.create_rectangle(x,y,x+50,y+50,fill="red")
                    x+=50
                else:
                    foundbox=0
                    for boxlist in boxes:
                        if([yy,xx] in win):
                            if([yy,xx] in boxes):
                                canvas.create_rectangle(x,y,x+50,y+50,fill="green")
                                x+=50 
                                foundbox=1
                                break
                            else:
                                canvas.create_rectangle(x,y,x+50,y+50,fill="yellow")
                                x+=50 
                                foundbox=1
                                break
                        elif(xx == boxlist[1] and yy == boxlist[0]):
                            canvas.create_rectangle(x,y,x+50,y+50,fill="grey")
                            x+=50 
                            foundbox=1
                    if not foundbox:
                        canvas.create_rectangle(x,y,x+50,y+50,fill="white")
                        x+=50
            if(arr_y==5):
                canvas.create_rectangle(x,y,x+50,y+50,fill="white")
                x+=50
            xx+=1
        y+=50
        x=0
        yy+=1
def push_box_check(direction):
    count = 0
    for box in boxes:
        if(box[1] == currentPosition[1] and box[0] == currentPosition[0]):
            if(direction == "left"):
                if(map_array[box[0]][box[1]-1]==1 and not [box[0],box[1]-1] in boxes):
                    boxes[count] = [box[0],box[1]-1]
                    break;
                else:
                    return 0
            if(direction == "right"):
                if(map_array[box[0]][box[1]+1]==1 and not [box[0],box[1]+1] in boxes):
                    boxes[count] = [box[0],box[1]+1]
                    break;
                else:
                    return 0
            if(direction == "up"):
                if(map_array[box[0]-1][box[1]]==1 and not [box[0]-1,box[1]] in boxes):
                    boxes[count] = [box[0]-1,box[1]]
                    break;
                else:
                    return 0
            if(direction == "down"):
                if(map_array[box[0]+1][box[1]]==1 and not [box[0]+1,box[1]] in boxes):
                    boxes[count] = [box[0]+1,box[1]]
                    break;
                else:
                    return 0
        count+=1
    return 1
    
                
def keydown_action():
    global currentPosition
    if(map_array[currentPosition[0]+1][currentPosition[1]]==1): #walkable
        currentPosition = [currentPosition[0]+1,currentPosition[1]]
    walkable = push_box_check("down")
    if not walkable:
        currentPosition = [currentPosition[0]-1,currentPosition[1]]
        print("dothis")
    canvas.delete("all")
    render()

def keyup_action():
    global currentPosition
    if(map_array[currentPosition[0]-1][currentPosition[1]]==1): #walkable
        currentPosition = [currentPosition[0]-1,currentPosition[1]]
    walkable = push_box_check("up")
    if not walkable:
        currentPosition = [currentPosition[0]+1,currentPosition[1]]
        print("dothis")
    canvas.delete("all")
    render()

def keyleft_action():
    global currentPosition
    if(map_array[currentPosition[0]][currentPosition[1]-1]==1): #walkable
        currentPosition = [currentPosition[0],currentPosition[1]-1]
    walkable = push_box_check("left")
    if not walkable:
        currentPosition = [currentPosition[0],currentPosition[1]+1]
        print("dothis")
    canvas.delete("all")
    render()

def keyright_action():
    global currentPosition
    if(map_array[currentPosition[0]][currentPosition[1]+1]==1): #walkable
        currentPosition = [currentPosition[0],currentPosition[1]+1]
    walkable = push_box_check("right")
    if not walkable:
        currentPosition = [currentPosition[0],currentPosition[1]-1]
        print("dothis")
    canvas.delete("all")
    render()

top = tk.Tk()
canvas = tk.Canvas(top,bg="white",height=450,width=450)
but = tk.Button(text="dd")
canvas.bind("<Down>",keydown)
canvas.bind("<Up>",keyup)
canvas.bind("<Left>",keyleft)
canvas.bind("<Right>",keyright)

# canvas.bind("<KeyPress>",keyup)
canvas.focus_set()
canvas.delete("all")
render()
canvas.pack()
but.pack()
top.mainloop()


