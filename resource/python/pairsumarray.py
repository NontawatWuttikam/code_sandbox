import sys
arr1 = [1,2,3,4,5]
arr2 = [2,4,5,7,9]
given = 100
diff = sys.maxsize*2+1
result = None
for i in arr1:
    for j in arr2:
        temp = i+j
        if abs(given - temp) < diff:
            diff = abs(given - temp)
            result = [i,j]

print(result)

